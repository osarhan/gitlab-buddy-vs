import * as vscode from "vscode";
import { CategoriesTreeDataProvider } from "./GitlabTreeProvider";

export function activate(context: vscode.ExtensionContext) {
  vscode.window.registerTreeDataProvider("gb-vs", new CategoriesTreeDataProvider());
}
export function deactivate() {}
