import * as vscode from "vscode";
import { getMergeRequests } from "./gitlabApi";
import { isAxiosError } from "axios";

export class CategoriesTreeDataProvider implements vscode.TreeDataProvider<Category> {
  constructor() {
    vscode.commands.registerCommand("gb-vs.refresh", () => this._refresh());

    vscode.commands.registerCommand("gb-vs.open_mr", (item) =>
      vscode.commands.executeCommand("vscode.open", vscode.Uri.parse(item.url))
    );
    vscode.commands.registerCommand("gb-vs.open_project", (item) =>
      vscode.commands.executeCommand("vscode.open", vscode.Uri.parse(item.url))
    );
    vscode.commands.registerCommand("gb-vs.open_pipeline", (item) =>
      vscode.commands.executeCommand("vscode.open", vscode.Uri.parse(item.url))
    );
  }
  private _onDidChangeTreeData: vscode.EventEmitter<Category | undefined> = new vscode.EventEmitter<
    Category | undefined
  >();
  readonly onDidChangeTreeData?: vscode.Event<Category | undefined> = this._onDidChangeTreeData.event;

  mergeRequests: any;
  pat = vscode.workspace.getConfiguration("gb-vs").get<string>("gitlabToken");
  url = vscode.workspace.getConfiguration("gb-vs").get<string>("gitlabUrl");

  private async _refresh() {
    this.pat = vscode.workspace.getConfiguration("gb-vs").get<string>("gitlabToken");
    this.url = vscode.workspace.getConfiguration("gb-vs").get<string>("gitlabUrl");
    console.log("refreshing merge requests...");
    if (!this.pat || !this.url) {
      vscode.window.showWarningMessage(
        "Gitlab Buddy requires you to set your Gitlab Token and URL in extension settings."
      );
      return;
    }
    const success = await this._fetchMergeRequests();
    if (!success) {
      return;
    }
    this._onDidChangeTreeData.fire(undefined);
  }

  private async _fetchMergeRequests() {
    try {
      this.mergeRequests = await getMergeRequests(this.pat, this.url);
      return true;
    } catch (e) {
      let message: string = "Failed Fetching Merge Requests. ";
      if (isAxiosError(e)) {
        if (e.code === "ECONNABORTED") {
          message = message + "Please check VPN/Connection";
        }
        if (e.code === "ENOTFOUND") {
          message = message + "Please check gitlab URL";
        }
        if (e.code === "ERR_BAD_REQUEST") {
          message = message + "Please check gitlab token";
        }
      } else {
        message = message + "Try restarting VSCode";
      }
      console.log(message);
      vscode.window.showErrorMessage(message);
      return false;
    }
  }

  getTreeItem(element: Category): vscode.TreeItem {
    return element;
  }

  async getChildren(element?: Category): Promise<Category[]> {
    if (!this.pat || !this.url) {
      vscode.window.showInformationMessage(
        "Gitlab Buddy requires you to set your Gitlab Token and URL in extension settings."
      );
      return [];
    }

    if (!element) {
      const success = await this._fetchMergeRequests();
      if (!success) {
        return [];
      }
      const categories = ["created", "assigned", "approved"];
      return categories.map((category) => {
        const mrs = this.mergeRequests[category];
        return {
          label: vscode.l10n.t(category.toUpperCase()),
          collapsibleState:
            mrs.length > 0 ? vscode.TreeItemCollapsibleState.Collapsed : vscode.TreeItemCollapsibleState.None,
          id: category,
          category: category,
          mrs: mrs,
          description: "(" + mrs.length.toString() + ")",
        };
      });
    } else if (element.category) {
      return element.mrs.map((mr: any) => ({
        label: mr.title,
        tooltip: mr.description,
        collapsibleState: vscode.TreeItemCollapsibleState.Collapsed,
        id: mr.id.toString(),
        mr: mr,
        description: element.category,
        contextValue: "mergeRequest",
        url: mr.web_url,
        iconPath: vscode.Uri.parse(mr.author.avatar_url),
      }));
    } else {
      return [
        new Project("Project", element.mr),
        new BuildStatus("Build Status", element.mr),
        new Comments("Comments", element.mr),
        new Approvers("Approvers", element.mr),
      ];
    }
  }
}

class Category extends vscode.TreeItem {
  constructor(
    name: string,
    collapsibleState: vscode.TreeItemCollapsibleState,
    public readonly mrs?: any,
    public readonly mr?: any,
    public readonly url?: any,
    public readonly category?: any
  ) {
    super(name, collapsibleState);
    this.collapsibleState = collapsibleState;
    this.label = vscode.l10n.t(name);
    this.tooltip = vscode.l10n.t(name);
    this.id = undefined;
  }
}

class BuildStatus extends vscode.TreeItem {
  constructor(name: string, public readonly mr: any, public readonly url?: any) {
    super(name);
    this.description = vscode.l10n.t("Pipeline Status");
    this.tooltip = mr.pipeline_data.detailed_status.label;
    this.iconPath = new vscode.ThemeIcon("rocket");
    this.label = vscode.l10n.t(
      mr.pipeline_data.status.charAt(0).toUpperCase() + mr.pipeline_data.status.slice(1)
    );
    this.collapsibleState = vscode.TreeItemCollapsibleState.None;
    this.contextValue = "pipeline";
    this.url = mr.pipeline_data.web_url;
  }
}

class Comments extends vscode.TreeItem {
  constructor(name: string, public readonly mr: any) {
    super(name);
    this.label = this._getCount(mr).toString();
    this.tooltip = name;
    this.description = vscode.l10n.t("Unresolved Threads");
    this.iconPath = new vscode.ThemeIcon("comment");
  }

  _getCount(mr: any) {
    let count = 0;
    function unresolvedNotes(notes: any) {
      let notesUnresolved = false;
      notes.forEach(function (n: any) {
        if (n.resolvable && !n.resolved) {
          notesUnresolved = true;
        }
      });
      return notesUnresolved;
    }

    if (mr.discussions) {
      mr.discussions.forEach(function (d: any) {
        if (d.notes && unresolvedNotes(d.notes)) {
          count += 1;
        }
      });
    }
    return count;
  }
}

class Approvers extends vscode.TreeItem {
  constructor(name: string, public readonly mr: any) {
    super(name);
    this.label = (mr.approved_by?.length || 0).toString();
    this.tooltip = vscode.l10n.t(name);
    this.description = vscode.l10n.t("Approvals");
    this.iconPath = new vscode.ThemeIcon("person-add");
  }
}

class Project extends vscode.TreeItem {
  constructor(name: string, public readonly mr: any, public readonly url?: any) {
    super(name);
    this.label = mr.project_name;
    this.tooltip = vscode.l10n.t(name);
    this.description = vscode.l10n.t("Project");
    this.iconPath = new vscode.ThemeIcon("repo");
    this.contextValue = "project";
    this.url = mr.project_url;
  }
}
