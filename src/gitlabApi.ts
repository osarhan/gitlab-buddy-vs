import keyBy from "lodash/keyBy";
import values from "lodash/values";
import orderBy from "lodash/orderBy";
import sortedUniqBy from "lodash/sortedUniqBy";
import axios from "axios";

export async function getMergeRequests(token: any, gitlabUrl: any) {
  const GITLAB_BASE_URL = gitlabUrl;
  let GITLAB_BASE_URL_HTTP = GITLAB_BASE_URL.startsWith("https://")
    ? GITLAB_BASE_URL
    : `https://${GITLAB_BASE_URL}`;

  const GITLAB_BASE_URL_API = GITLAB_BASE_URL_HTTP.endsWith("/")
    ? `${GITLAB_BASE_URL_HTTP}api/v4`
    : `${GITLAB_BASE_URL_HTTP}/api/v4`;

  // eslint-disable-next-line @typescript-eslint/naming-convention
  const headers = { Accept: "application/json", Authorization: `Bearer ${token}` };

  const gitlabKyClient = axios.create({
    baseURL: GITLAB_BASE_URL_API,
    headers: headers,
    timeout: 8000,
  });

  async function getUserInfo() {
    const url = "user";
    return gitlabKyClient.get(url).then((resp: any) => resp.data);
  }

  const userData: any = await getUserInfo();

  async function getMrs() {
    const urlToTypeDict: any = {
      au: "Author",
      as: "Assignee",
      re: "Reviewer",
      ap: "Group Approvals",
    };
    const urls: string[] = [
      `merge_requests?author_id=${userData.id}&state=opened&per_page=100&scope=all&non_archived=true&view=simple`,
      `merge_requests?assignee_id=${userData.id}&state=opened&per_page=100&scope=all&non_archived=true&view=simple`,
      `merge_requests?reviewer_id=${userData.id}&state=opened&per_page=100&scope=all&non_archived=true&view=simple`,
      `merge_requests?approver_ids[]=${userData.id}&state=opened&per_page=100&scope=all&non_archived=true&view=simple`,
    ];
    const res: any = [];
    await Promise.all(
      urls.map((url) =>
        gitlabKyClient
          .get(url)
          .then((resp: any) => resp.data)
          .then((mrs: any) => {
            mrs.forEach((m: any) => {
              const mr = m;
              mr.mrType = urlToTypeDict[url.slice(16, 18)];
              res.push(mr);
            });
          })
      )
    );
    return res;
  }

  async function getMergeRequestData(mrsD: any, mrs: any) {
    const mrsDict = mrsD;
    const urls: string[] = [];
    mrs.forEach((mr: any) => {
      urls.push(
        `projects/${mr.project_id}/merge_requests/${mr.iid}`,
        `projects/${mr.project_id}/merge_requests/${mr.iid}/approvals`,
        `projects/${mr.project_id}/merge_requests/${mr.iid}/discussions?per_page=100`
      );
    });
    await Promise.all(
      urls.map((url: string) =>
        gitlabKyClient.get(url).then((r: any) => {
          const resp = r.data;
          const splitUrl = url.split("/");
          const mrKey = `${splitUrl[1]}-${splitUrl[3]}`;
          if (splitUrl.length === 4) {
            // pipeline data
            mrsDict[mrKey].author = resp.author;
            mrsDict[mrKey].pipeline_data = resp.head_pipeline;
            mrsDict[mrKey].project_name = resp.references.full.split("/").at(-1).split("!")[0];
            mrsDict[mrKey].project_url = `${GITLAB_BASE_URL_HTTP}/${resp.references.full.split("!")[0]}`;
            mrsDict[mrKey].labels = resp.labels;
          } else if (splitUrl[4] === "approvals") {
            // approvals
            mrsDict[mrKey].approved_by = resp.approved_by;
          } else {
            // discussion
            mrsDict[mrKey].discussions = resp;
          }
        })
      )
    );
    return mrsDict;
  }

  function formatApprovers(mr: any) {
    let isApproved = false;
    mr.approved_by.forEach((approvedBy: any) => {
      if (approvedBy.user.id === userData.id) {
        isApproved = true;
      }
    });
    return isApproved;
  }

  let mrs = await getMrs();
  let mrsDict = keyBy(mrs, function buildDict(o) {
    return `${o.project_id}-${o.iid}`;
  });
  mrsDict = await getMergeRequestData(mrsDict, mrs);
  mrs = values(mrsDict);
  mrs = orderBy(mrs, "created_at", "desc");
  mrs = sortedUniqBy(mrs, "id");

  const approved: any = [];
  const assigned: any = [];
  const created: any = [];
  mrs.forEach((mr: any) => {
    if (mr.author.id === userData.id) {
      return created.push(mr);
    }
    const isApproved = formatApprovers(mr);
    if (isApproved) {
      return approved.push(mr);
    }
    assigned.push(mr);
  });

  const mergeRequests = {
    approved,
    assigned,
    created,
  };
  return mergeRequests;
}
