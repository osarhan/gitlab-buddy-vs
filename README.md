# Gitlab Buddy README

## Features

The merge request treeview Gitlab is missing! Never miss an assigned merge request ever again! This finds all assigned MRs, and displays in activitybar for quick access without having to manually subscribe to each repository. Quickly see what you are assigned to as an assignee, reviewer, or part of a group approval rule.

Throw away your gitlab search queries!

![Demo Picture](demopic.png)

![Demo Gif](demogif.gif)

Features include:

- Grabs merged requests under group approvals, assignee, reviewer and created
- Pending/Approved/Created MRs
- Direct link to merge request/Repo/Author and Pipelines
- See immediately if approved and by who
- CI/CD pipeline status
- Unresolved threads status

Future Features:

- Filter by draft/ready status
- Hide merge requests from list
- Merge conflict status

## Requirements

Ability to generate a personal access token with read access.

## Extension Settings

This extension contributes the following settings:

- `gb-vs.gitlabUrl`: Gitlab Url instance. (www.gitlab.com) Works with enterprise instances!
- `gb-vs.gitlabToken`: Gitlab Personal Access Token, used to access gitlab. Read access required.

Made with ❤️ by Omar Sarhan
