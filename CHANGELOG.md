# Change Log

All notable changes to the "gb-vs" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [0.19.0]

- Initial release

All previous versions were testing/debugging/enhancements and lots of learning
